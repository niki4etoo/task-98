import React from 'react';
import LoginFormStyles from './LoginForm.module.css';

export default function LoginForm(){

	return (
	<form className={LoginFormStyles.form}>
		<label className={LoginFormStyles.label}>Name</label>
		<input
		className={LoginFormStyles.input}
		id="name"
		type="text"
		 />
		 <label className={LoginFormStyles.label}>Password</label>
		 <input
		 className={LoginFormStyles.input}
		 id="password"
		 type="password"
		 />
		 <button className={LoginFormStyles.submit}>Submit</button>
	</form>
	);

}
